//
//  DefaultScreenViewController.m
//  Cambium
//
//  Created by ScriptLanes on 26/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import "DefaultScreenViewController.h"
#import "Constants.h"



@interface DefaultScreenViewController ()

@end

@implementation DefaultScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view addSubview:recordingInterrupted];
    [self.view addSubview:launchBrowser];
    launchBrowser.hidden = YES;
    recordingInterrupted.hidden = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@",[defaults valueForKey:@"RecordingInterrupted"]);
    if ([[defaults valueForKey:@"RecordingInterrupted"] integerValue] == 1)
    {
        recordingInterrupted.hidden = NO;
        launchBrowser.hidden = YES;
    }
    else
    {
        recordingInterrupted.hidden = YES;
        launchBrowser.hidden = NO;
        
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onClickLanuchBrowser:(id)sender
{
   
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:productionURL]];
  
}

- (IBAction)onClickTryAgain:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:@"RedirectURL"] != nil)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[defaults valueForKey:@"RedirectURL"]]];
    }
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight ||
            interfaceOrientation == UIInterfaceOrientationLandscapeLeft );
}

@end
