//
//  AppDelegate.m
//  Cambium
//
//  Created by ScriptLanes on 15/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "DefaultScreenViewController.h"
#import "FirstViewController.h"
#import "Constants.h"
#import "Crittercism.h"

@implementation AppDelegate
@synthesize defaultviewController,currentDate;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"didFinish");
    
    [self performSelector:@selector(loadDefaultScreen) withObject:nil afterDelay:1.0];
    [Crittercism enableWithAppID: @"52313be446b7c221bc000007"];
    

    return YES;
    
    
}

-(void)loadDefaultScreen
{
    if (!self.window)
    {
        if (self.defaultviewController)
        {
            defaultviewController = nil;
        }

        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];        
        [self.window makeKeyAndVisible];
        defaultviewController = [[DefaultScreenViewController alloc] initWithNibName:@"DefaultScreenViewController" bundle:nil];
        self.window.rootViewController = defaultviewController;
    }
       
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    // Do something with the url here
    NSLog(@"launchURL");
    
    NSLog(@"%@",url);
    if (url != nil)
    {
        currentDate = [NSDate date];        
        NSString *str = [url absoluteString];
        NSArray *splitUrl = nil;
        splitUrl = [str componentsSeparatedByString:@"VSESSIONID="];
        splitUrl = [[splitUrl objectAtIndex:1] componentsSeparatedByString:@"&"];
        NSLog(@"%@",[splitUrl objectAtIndex:0]);
        
        NSArray *redirectUrl = nil;
        redirectUrl = [str componentsSeparatedByString:@"redirectUrl="];
        redirectUrl = [[redirectUrl objectAtIndex:1] componentsSeparatedByString:@"&"];
        NSLog(@"%@",[redirectUrl objectAtIndex:0]);
               
        NSArray *bundleID = nil;
        bundleID = [str componentsSeparatedByString:@"bundleUuid="];
        bundleID = [[bundleID objectAtIndex:1] componentsSeparatedByString:@"&"];
        NSLog(@"%@",[bundleID objectAtIndex:0]);
        
        NSArray *baseURLArray = nil;
        baseURLArray = [str componentsSeparatedByString:@"baseUrl="];
        NSLog(@"%@",[baseURLArray lastObject]);
        
        NSArray *initialURLArray = nil;
        initialURLArray = [str componentsSeparatedByString:@"initialUrl="];
        initialURLArray = [[initialURLArray objectAtIndex:1] componentsSeparatedByString:@";"];
        NSLog(@"%@",[initialURLArray lastObject]);
        
        
        if ([splitUrl objectAtIndex:0] != nil || ![[splitUrl objectAtIndex:0] isEqualToString:@""])
        {
            
            NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
            [standardDefaults setObject:[splitUrl objectAtIndex:0] forKey:@"SessionId"];
            [standardDefaults setObject:[redirectUrl objectAtIndex:0] forKey:@"RedirectURL"];
            [standardDefaults setObject:[bundleID objectAtIndex:0] forKey:@"BundleUuid"];
            [standardDefaults setObject:[baseURLArray lastObject] forKey:@"BaseURL"];
            [standardDefaults setObject:[initialURLArray objectAtIndex:0] forKey:@"initialURL"];
            [standardDefaults setBool:YES forKey:@"RecordingInterrupted"];
            
             NSLog(@"NSD %@",[standardDefaults valueForKey:@"RecordingInterrupted"]);
            
            baseURL = [baseURLArray lastObject];
            redirectURL = [redirectUrl objectAtIndex:0];
            authenticationURL = [[initialURLArray objectAtIndex:0] stringByAppendingString:@";"];
            
            
           // [standardDefaults setObject:@"1da402c7-903e-5dfe-3efe-da2c24eefa78" forKey:@"BundleUuid"];
            [standardDefaults synchronize];
            
            if (!self.window) {
                self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                [self.window makeKeyAndVisible];
            }
            
            
            // Override point for customization after application launch.
            
            if (self.viewController)
            {
                [self.viewController my_viewWillUnload];
                [self.viewController removeFromParentViewController];
                self.viewController = nil;
            }
            self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
            self.window.rootViewController = self.viewController;
            
            
            
        }
    }
    
   
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{  
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if (self.viewController)
    {
        [self.viewController my_viewWillUnload];
        [self.viewController removeFromParentViewController];
        self.viewController = nil;
    }
    
    NSString *filePath =[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:nil];
    
    [[NSThread currentThread] cancel];
     
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
           
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if (self.defaultviewController)
    {
        defaultviewController = nil;
    }
    
    if (self.viewController == nil)
    {
        
        defaultviewController = [[DefaultScreenViewController alloc] initWithNibName:@"DefaultScreenViewController" bundle:nil];
        self.window.rootViewController = defaultviewController;
    }        
    

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
