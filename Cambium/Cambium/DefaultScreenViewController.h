//
//  DefaultScreenViewController.h
//  Cambium
//
//  Created by ScriptLanes on 26/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefaultScreenViewController : UIViewController
{
    IBOutlet UIView *launchBrowser;
    IBOutlet UIView *recordingInterrupted;
}

-(IBAction)onClickLanuchBrowser:(id)sender;

@end
