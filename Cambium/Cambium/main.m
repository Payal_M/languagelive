//
//  main.m
//  Cambium
//
//  Created by ScriptLanes on 15/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
