//
//  ViewController.h
//  Cambium
//
//  Created by ScriptLanes on 15/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#include "lame.h"



@interface ViewController : UIViewController<AVAudioRecorderDelegate,AVAudioPlayerDelegate,UIAlertViewDelegate>
{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    
    NSMutableDictionary *wordDictionary;
    NSMutableDictionary *phonemeDictionary;
    NSMutableDictionary *sentenceDictionary;
    NSMutableDictionary *passageDictionary;
    
    IBOutlet UIView *loadingView;
    IBOutlet UIView *youStillThereView;
    IBOutlet UIView *uploadingRecord;
    IBOutlet UIView *recordingComplete;
    IBOutlet UIView *recordingInterrupted;
    IBOutlet UIView *startRecording;
    IBOutlet UIView *displayQuestionsView;
    IBOutlet UILabel *displayQuestionLabel;
    IBOutlet UILabel *decreasingSeconds;
        
    IBOutlet UIButton *stopRecordingButton;
    IBOutlet UILabel *stopRecordingLabel;
    IBOutlet UIImageView *clockImage;
    IBOutlet UIImageView *continueImage;
    IBOutlet UILabel *showTime;
    IBOutlet UILabel *recordingCompletedLabel;
    IBOutlet UIProgressView *progressView;
    IBOutlet UIImageView *mic_image;
    
    
    
    int phonemeCount,wordCount,sentencesCount,secondsCount,recordingSecondsCount,passageDuration,micValue,stopRecordingTime,passageValue,totalCount;
    
    NSString *fileName;
    NSTimer* timer , *timeOut ,*decreasingTimer,*recordingTimer,*finalTimer;
    UIImageView *image1,*image2,*image3,*image4,*image5,*image6,*image7,*image8,*image9,*image10,*image11,*image12,*image13,*image14,*image15,*image16,*image17,*image18,*image19,*image20;
    
    UILabel *label1,*label2,*label3;
    
    int i;
    
    float totalRecordingTime;
    NSString *durationValue;
    
    NSMutableArray *pathArray;
    
    NSMutableDictionary *wordPathDictionary,*phonemePathDictionary,*sentencesPathDictioanry;
    bool stopFlag;
    
    NSDate *previousDate ,*endDate;
    
    

}

@property (strong , nonatomic)NSDate *previousDate;
@property (strong , nonatomic)NSDate *endDate;

- (IBAction)recordPauseTapped:(id)sender;
- (IBAction)redirectToSafari:(id)sender;
- (IBAction)onClickContinue:(id)sender;
- (IBAction)onClickStopRecording:(id)sender;
- (IBAction)onClickTryAgain:(id)sender;

-(void)activityWords;
-(void)activitySentences;
-(void)activityPhonemes;
-(void)activityPassage;
-(void)fetchWords;

-(void)recordingCompleted;
-(void)countDownSeconds;
-(void)hightlightImages:(int)imageCount :(BOOL)value :(int) totalCount1;
-(void)hideImages;

-(void)authenticateUser;


-(void)initRecording:(NSString *)fileName;
-(void)convertTapped:(NSDictionary *)info;
-(void)convertMp3Finish:(NSDictionary *)info;
-(void)uploadFile:(NSDictionary *)info;
-(void)stopActivityRecording:(NSDictionary *)info;
-(void)startActivityRecording:(NSDictionary *)info;
-(void)toMp3:(NSDictionary *)info;
-(void)my_viewWillUnload;


@end
