//
//  ViewController.m
//  Cambium
//
//  Created by ScriptLanes on 15/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking.h>
#import <JSONKit.h>
#import "Constants.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize previousDate,endDate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self cleanUp];
    
    
    
    [self.view addSubview:loadingView];
    [self.view addSubview:youStillThereView];
    [self.view addSubview:uploadingRecord];
    [self.view addSubview:recordingComplete];
    [self.view addSubview:recordingInterrupted];
    [self.view addSubview:startRecording];
    [self.view addSubview:displayQuestionsView];

    [loadingView setFrame:CGRectMake(307, 200, 411, 345)];
    [youStillThereView setFrame:CGRectMake(307, 200, 411, 345)];
    [uploadingRecord setFrame:CGRectMake(307, 200, 411, 345)];
    [recordingComplete setFrame:CGRectMake(307, 200, 411, 345)];
    [recordingInterrupted setFrame:CGRectMake(307, 200, 411, 345)];
    [startRecording setFrame:CGRectMake(307, 200, 411, 345)];
    [displayQuestionsView setFrame:CGRectMake(0, 42, 1024, 748)];
        
    
    loadingView.hidden = NO;
    youStillThereView.hidden = YES;
    uploadingRecord.hidden = YES;
    recordingComplete.hidden = YES;
    recordingInterrupted.hidden =YES;
    startRecording.hidden = YES;
    displayQuestionsView.hidden = YES;
    
    stopRecordingButton.hidden = YES;
    stopRecordingLabel.hidden = YES;
    continueImage.hidden = YES;
    clockImage.hidden = YES;
    showTime.hidden = YES;
    progressView.hidden = YES;
    
    
    secondsCount = 30;
    i = 0;
   
    [self authenticateUser];

    
    pathArray = [[NSMutableArray alloc] init];
    
    int xValue = 300;
    
    image1 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 650, 7, 7)];
    [displayQuestionsView addSubview:image1];
    
    image2 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image2];
    xValue = xValue + 20;
    
    image3 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image3];
    xValue = xValue + 20;
    image4 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image4];
    xValue = xValue + 20;
    
    image5 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image5];
    xValue = xValue + 20;
    
    image6 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image6];
    xValue = xValue + 20;
    
    image7 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image7];
    xValue = xValue + 20;
    
    
    image8 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image8];
    xValue = xValue + 20;
    image9 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image9];
    xValue = xValue + 20;
    
    image10 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image10];
    xValue = xValue + 20;
    
    image11 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image11];
    xValue = xValue + 20;
    
    image12 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image12];
    xValue = xValue + 20;
    
    image13 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image13];
    xValue = xValue + 20;
    
    image14 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image14];
    xValue = xValue + 20;
    
    image15 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image15];
    xValue = xValue + 20;
    
    image16 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image16];
    xValue = xValue + 20;
    
    image17 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image17];
    xValue = xValue + 20;
    
    image18 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image18];
    xValue = xValue + 20;
    
    image19 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image19];
    xValue = xValue + 20;
    
    image20 = [[UIImageView alloc] initWithFrame:CGRectMake(xValue + 20, 650, 7, 7)];
    [displayQuestionsView addSubview:image20];
    xValue = xValue + 20;
    
    label1 = [[UILabel alloc] initWithFrame:CGRectMake(xValue + 20, 640, 20, 20)];
    [displayQuestionsView addSubview:label1];
    xValue = xValue + 20;
    
    label2 = [[UILabel alloc] initWithFrame:CGRectMake(xValue + 20 , 640, 20, 20)];
    [displayQuestionsView addSubview:label2];
    xValue = xValue + 20;
    
    label3 = [[UILabel alloc] initWithFrame:CGRectMake(xValue + 10, 640, 20, 20)];
    [displayQuestionsView addSubview:label3];
    xValue = xValue + 20;

    
    [label1 setBackgroundColor:[UIColor clearColor]];
    [label2 setBackgroundColor:[UIColor clearColor]];
    [label3 setBackgroundColor:[UIColor clearColor]];
    
     [self hightlightImages:0 :NO :0];
    
    micValue = 0;
   

   
    
    
}
-(void)micAnimation
{
    
    [UIView animateWithDuration:1.0f delay:0.5f options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat    animations:^
     {
         switch (micValue)
         {
             case 0:
                
                 [mic_image setImage:[UIImage imageNamed:@"step_0"]];
                 break;
             case 1:
               
                 [mic_image setImage:[UIImage imageNamed:@"step_1"]];
                 break;
             case 2:
               
                 [mic_image setImage:[UIImage imageNamed:@"step_2"]];
                 break;
             case 3:
                
                 [mic_image setImage:[UIImage imageNamed:@"step_3"]];
                 break;
                 
             default:
                 break;
         }
         
     } completion:^(BOOL finished)
     {
         
         micValue++;
         if (micValue == 4)
         {
             micValue = 0;
         }
         
         if (stopFlag == NO)
         {
              [self performSelector:@selector(micAnimation) withObject:nil afterDelay:0.5f];
         }
         else
         {
             micValue = 0;
         }
       
         
         
     }];
    

    
}
-(void)viewWillAppear:(BOOL)animated
{
       
}

#pragma mark - Complete parsing of data and start recording

-(void)downloadingComplete
{
   
    loadingView.hidden = YES;
    youStillThereView.hidden = YES;
    uploadingRecord.hidden = YES;
    recordingComplete.hidden = YES;
    recordingInterrupted.hidden =YES;
    startRecording.hidden = NO;
    displayQuestionsView.hidden = YES;
    timeOut = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(informTimeOut) userInfo:nil repeats:NO];
    
    
}
-(void)informTimeOut
{
    loadingView.hidden = YES;
    youStillThereView.hidden = NO;
    uploadingRecord.hidden = YES;
    recordingComplete.hidden = YES;
    recordingInterrupted.hidden =YES;
    startRecording.hidden = YES;
    displayQuestionsView.hidden = YES;
    [timeOut invalidate];
    secondsCount = 30;
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownSeconds) userInfo:@"You will be returned to the browser in" repeats:YES];
        
    
}
-(void)countDownSeconds
{
    
    secondsCount -- ;
    if (secondsCount > 0)
    {
        decreasingSeconds.text = [NSString stringWithFormat:@"%@ %d seconds",[timer userInfo],secondsCount];
       
    }
    else if (secondsCount == 0)
    {
        
        decreasingSeconds.text = [NSString stringWithFormat:@"%@ %d seconds",[timer userInfo],secondsCount];
      
        [timer invalidate];
        NSString *url = redirectURL;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    
}

-(void)countDownForRecordingComplete
{
    
    recordingSecondsCount -- ;
    if (recordingSecondsCount > 0)
    {
        
        recordingCompletedLabel.text = [NSString stringWithFormat:@"%@ %d seconds",[decreasingTimer userInfo],recordingSecondsCount];
    }
    else if (recordingSecondsCount == 0)
    {
        NSLog(@"redirecting to browser");
        decreasingSeconds.text = [NSString stringWithFormat:@"%@ %d seconds",[decreasingTimer userInfo],recordingSecondsCount];
       
        [decreasingTimer invalidate];
        NSString *url =   redirectURL;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
    
}
- (IBAction)onClickContinue:(id)sender
{
    
    [timer invalidate];
    loadingView.hidden = NO;
    youStillThereView.hidden = YES;
    uploadingRecord.hidden = YES;
    recordingComplete.hidden = YES;
    recordingInterrupted.hidden =YES;
    startRecording.hidden = YES;
    displayQuestionsView.hidden = YES;
    
    
    stopRecordingButton.hidden =YES;
    stopRecordingLabel.hidden = YES;
    continueImage.hidden = YES;
    clockImage.hidden = YES;
    showTime.hidden = YES;
    
    
   
    
    [self authenticateUser];
}


#pragma mark - start recording


- (IBAction)recordPauseTapped:(id)sender {
    // Stop the audio player before recording
    loadingView.hidden = YES;
    youStillThereView.hidden = YES;
    uploadingRecord.hidden = YES;
    recordingComplete.hidden = YES;
    recordingInterrupted.hidden =YES;
    startRecording.hidden = YES;
    displayQuestionsView.hidden = NO;
    [timeOut invalidate];
    
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording)
    {
       
        wordCount = 0;
        wordPathDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];
        if ([[wordDictionary valueForKey:@"lessonType"] intValue]  == words)
        {
            wordCount = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count];
            NSLog(@"%d",wordCount);
            [self activityWords];
        }
        else if ([[wordDictionary valueForKey:@"lessonType"]intValue] == phonemes)
        {
            wordCount = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count];
            NSLog(@"%d",wordCount);
            [self activityPhonemes];
        }
        else if ([[wordDictionary valueForKey:@"lessonType"]intValue] == sentences)
        {
            wordCount = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count];
            NSLog(@"%d",wordCount);
            [self activitySentences];
        }
        else if ([[wordDictionary valueForKey:@"lessonType"]intValue] == passage)
        {
            wordCount = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count];
            [self activityPassage];
        }
        
       totalCount = wordCount;
                
    } else
    {
        
       
        [recorder pause];
       
    }
    
  
}

- (IBAction)stopTapped:(id)sender {
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (IBAction)playTapped:(id)sender {
    if (!recorder.recording){
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
    }
}




-(void)initRecording:(NSString *)fileName1
{
    
    
    
    //[stopButton setEnabled:NO];
    //[playButton setEnabled:NO];
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               fileName1,
                               nil];
    
    NSLog(@"%@",pathComponents);
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    
    // Initiate and prepare the recorder
    if (recorder) {
        [recorder stop];
        recorder.delegate = nil;
        recorder = nil;
    }
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
}
-(void)startActivityRecording:(NSDictionary *)info
{
    NSLog(@"start");
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    [session setActive:YES error:nil];
     [recorder record];
    if ([[wordDictionary valueForKey:@"lessonType"]intValue] == passage)
    {
        
        [self performSelector:@selector(stopActivityRecording:) withObject:info afterDelay:stopRecordingTime];
    }
    else
    {
        //4
        [self performSelector:@selector(stopActivityRecording:) withObject:info afterDelay:stopRecordingTime];
    }
    
        
    
}
-(void)stopActivityRecording :(NSDictionary *)info
{
    
        [recorder stop];
        NSLog(@" %d stop",passageDuration);
        [self performSelector:@selector(convertTapped:) withObject:info afterDelay:0.0];
        stopFlag = YES;
        [self.view.layer removeAllAnimations];
    
    
    
   
}

#pragma mark - fade in fade out animation

-(void)curveEasyInButton:(UILabel *)button withDuration:(float)duration andDelay:(float)delay
{
       
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^
    {         
        [button setAlpha:1.0];
        
    } completion:^(BOOL finished)
    {
       
        
        //5.0
        [self curveEasyOutButton:button withDuration:curveOutDuration andDelay:stopRecordingTime];
            
    }];
    
}

-(void)curveEasyOutButton:(UILabel *)button withDuration:(float)duration andDelay:(float)delay
{
        
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^
    {
        
        [button setAlpha:0.0];
    }
     
    completion:^(BOOL finished)
    {
        
        
            if ([[wordDictionary valueForKey:@"lessonType"] intValue]  == words)
            {
                wordCount--;
                if (wordCount > 0)
                {
                    
                    
                    [self activityWords];
                    
                    
                }
                else if (wordCount == 0)
                {
                    
                    
                    [self recordingCompleted];
                    
                }

            }
            else if ([[wordDictionary valueForKey:@"lessonType"] intValue]  == phonemes)
            {
                wordCount--;
                if (wordCount > 0 )
                {
                    [self activityPhonemes];
                }
                else if (wordCount == 0)
                {
                    
                    
                    [self recordingCompleted];
                }

            }
            else if ([[wordDictionary valueForKey:@"lessonType"] intValue]  == sentences)
            {
                 wordCount --;
                if (wordCount > 0)
                {
                   
                    [self activitySentences];
                   
                }
                else if (wordCount == 0)
                {
                    
                    [self recordingCompleted];
                   
                }

            }
        
        
        

     
    }];
    
    
}

#pragma mark - Show one by one letters words sentences

-(void)recordingCompleted
{
    
    NSLog(@"recording complete");    
    
    [self uploadAllFiles];
    
}
-(void)doneAllUploading
{
    loadingView.hidden = YES;
    youStillThereView.hidden = YES;
    uploadingRecord.hidden = YES;
    recordingComplete.hidden = NO;
    recordingInterrupted.hidden = YES;
    startRecording.hidden = YES;
    displayQuestionsView.hidden = YES;
    
    stopRecordingButton.hidden = YES;
    stopRecordingLabel.hidden = YES;
    continueImage.hidden = YES;
    clockImage.hidden = YES;
    showTime.hidden = YES;
    recordingSecondsCount = 30;
    decreasingTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownForRecordingComplete) userInfo:@"You will be redirected to review your work in" repeats:YES];

}

- (IBAction)onClickStopRecording:(id)sender
{
    [recorder stop];
    [recordingTimer invalidate];
    recordingTimer = nil;
    
    loadingView.hidden = YES;
    youStillThereView.hidden = YES;
    uploadingRecord.hidden = NO;
    recordingComplete.hidden = YES;
    recordingInterrupted.hidden = YES;
    startRecording.hidden = YES;
    displayQuestionsView.hidden = YES;
    
    stopRecordingButton.hidden = YES;
    stopRecordingLabel.hidden = YES;
    continueImage.hidden = YES;
    clockImage.hidden = YES;
    showTime.hidden = YES;
    
   // [self performSelector:@selector(convertTapped:) withObject:@"passage" afterDelay:0.0];
    

    
}
- (IBAction)redirectToSafari:(id)sender
{
    
    recordingSecondsCount = 0;
    // https://dev.languagelive.voyagerlearning.com/assignment/?view=dashboard
    NSString *url = redirectURL;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}



- (IBAction)onClickTryAgain:(id)sender
{
   // [self authenticateUser];
    NSString *url = redirectURL;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void)showPassageTime
{
    
    if ([recordingTimer isValid])
    {
        if (passageValue != 0)
        {
            
            progressView.progress = (float)passageValue/passageDuration;
            showTime.text = [NSString stringWithFormat:@"%d",passageValue];
            passageValue --;
        }
        else
        {
            passageValue --;
            showTime.text = [NSString stringWithFormat:@"%d",passageValue];
            [recordingTimer invalidate];
            
            
            loadingView.hidden = YES;
            youStillThereView.hidden = YES;
            uploadingRecord.hidden = NO;
            recordingComplete.hidden = YES;
            recordingInterrupted.hidden = YES;
            startRecording.hidden = YES;
            displayQuestionsView.hidden = YES;
            
            stopRecordingButton.hidden = YES;
            stopRecordingLabel.hidden = YES;
            continueImage.hidden = YES;
            clockImage.hidden = YES;
            showTime.hidden = YES;
            progressView.hidden = YES;
            
            
            
        }

    }
    
   }

-(void)activityPassage
{
    
    stopRecordingButton.hidden = NO;
    stopRecordingLabel.hidden = NO;
    continueImage.hidden = NO;
    clockImage.hidden = NO;
    showTime.hidden = NO;
    progressView.hidden = NO;
    
    
    passageDuration = 0;
    [self hideImages];
    
    [displayQuestionLabel setFont:[UIFont fontWithName:@"Helvetica Regular" size:28]];
    displayQuestionLabel.textAlignment = UITextAlignmentLeft;    
    durationValue = [wordDictionary objectForKey:@"duration"] ;
    
    passageDuration = [[wordDictionary objectForKey:@"duration"]  integerValue];
   
    passageDuration = passageDuration / 1000;
    passageValue = passageDuration;
    stopRecordingTime = passageDuration;
    fileName = [NSString stringWithFormat:@"passage"];
    
    NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];
    
    [infoDic setObject:[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:0] forKey:@"CurrentComponent"];
    [infoDic setObject:fileName forKey:@"FileName"];
    
    [displayQuestionLabel setAlpha:1.0];
    displayQuestionLabel.text = [[[[wordDictionary objectForKey:@"questions"] valueForKey:@"question"] objectAtIndex:0] valueForKey:@"passage"];
    [self initRecording:fileName];
    [self performSelector:@selector(startActivityRecording:) withObject:infoDic afterDelay:0];
    [self performSelector:@selector(micAnimation) withObject:nil afterDelay:0];
    
     recordingTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showPassageTime) userInfo:nil repeats:YES];
    
    
}


-(void)activityWords
{
    //static int i=0;
    stopRecordingTime = 0;
   [displayQuestionLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:90]];    
    durationValue = [wordDictionary valueForKey:@"duration"];    
    stopRecordingTime = [[wordDictionary valueForKey:@"duration"] integerValue];
    stopRecordingTime = stopRecordingTime / 1000;
       
    if (wordCount != 0)
    {
         NSLog(@"%d recording time %d",wordCount,stopRecordingTime);
        stopFlag = NO;
        fileName = [NSString stringWithFormat:@"word%d",i];
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];        
        [infoDic setObject:[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:i]
                    forKey:@"CurrentComponent"];
        [infoDic setObject:fileName forKey:@"FileName"];
        displayQuestionLabel.text = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:i] ;
        [self initRecording:fileName];
        [self curveEasyInButton:displayQuestionLabel withDuration:curveInDuration andDelay:0];
        [self performSelector:@selector(startActivityRecording:) withObject:infoDic afterDelay:curveInDuration];
        [self performSelector:@selector(micAnimation) withObject:nil afterDelay:curveInDuration];
        [self hightlightImages:i :YES :[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] count]];
        i++;
    }
    
    if (i ==  [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count])
    {
        i = 0;
        NSLog(@"value of i set to 0");
        NSLog(@"%d",wordCount);
    }

   
}
-(void)activitySentences
{
    //static int i=0;
    NSLog(@" i %d",i);
    [displayQuestionLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:32]];
     durationValue = [wordDictionary valueForKey:@"duration"];
  
    
    if (wordCount != 0)
    {
        NSLog(@"%d",wordCount);
        stopFlag = NO;
        stopRecordingTime = 0;
        stopRecordingTime = [[wordDictionary valueForKey:@"duration"] integerValue];
        stopRecordingTime = stopRecordingTime / 1000;
               

         NSLog(@"%d recording time %d",wordCount,stopRecordingTime);
        
        fileName = [NSString stringWithFormat:@"sentences%d",i];
        
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];
        
        [infoDic setObject:[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:i] forKey:@"CurrentComponent"];
        [infoDic setObject:fileName forKey:@"FileName"];

        displayQuestionLabel.text = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:i] ;
        [self initRecording:fileName];
        [self curveEasyInButton:displayQuestionLabel withDuration:curveInDuration andDelay:0];
        [self performSelector:@selector(startActivityRecording:) withObject:infoDic afterDelay:curveInDuration];
        [self performSelector:@selector(micAnimation) withObject:nil afterDelay:curveInDuration];
        [self hightlightImages:i :YES :[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count]];
        i++;
        
    }
    if (i ==  [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count])
    {
        i = 0;
        NSLog(@"value of i set to 0");
        NSLog(@"%d",wordCount);
    }
    
}


-(void)activityPhonemes
{
   // static int i=0;
    [displayQuestionLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:90]];
    
    
    if (wordCount != 0 || wordCount > 0)
    {
        durationValue = [wordDictionary valueForKey:@"duration"];
        stopFlag = NO;
        stopRecordingTime = 0;
        stopRecordingTime = [[wordDictionary valueForKey:@"duration"] integerValue];
        stopRecordingTime = stopRecordingTime / 1000;
        fileName = [NSString stringWithFormat:@"phoneme%d",i];        
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];        
        [infoDic setObject:[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:i] forKey:@"CurrentComponent"];
        [infoDic setObject:fileName forKey:@"FileName"]; 
        displayQuestionLabel.text = [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] objectAtIndex:i];
        [self initRecording:fileName];
        [self curveEasyInButton:displayQuestionLabel withDuration:curveInDuration andDelay:0];
        [self performSelector:@selector(startActivityRecording:) withObject:infoDic afterDelay:curveInDuration];
        [self performSelector:@selector(micAnimation) withObject:nil afterDelay:curveInDuration];
        [self hightlightImages:i :YES :[[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"] count]];
        i++;
        
       
    }
    if (i ==  [[[wordDictionary objectForKey:@"questions"] valueForKey:@"component"]  count])
    {
        i = 0;
        NSLog(@"value of i set to 0");
        NSLog(@"%d",wordCount);
    }
        
}


#pragma mark - show number of words count
-(void)hideImages
{
    
    image1.hidden = YES;
    image2.hidden = YES;
    image3.hidden = YES;
    image4.hidden = YES;
    image5.hidden = YES;
    image6.hidden = YES;
    image7.hidden = YES;
    image8.hidden = YES;
    image9.hidden = YES;
    image10.hidden = YES;
    image11.hidden = YES;
    image12.hidden = YES;
    image13.hidden = YES;
    image14.hidden = YES;
    image15.hidden = YES;
    image16.hidden = YES;
    image17.hidden = YES;
    image18.hidden = YES;
    image19.hidden = YES;
    image20.hidden = YES;
    label1.hidden = YES;
    label2.hidden = YES;
    label3.hidden = YES;
    
}


-(void)hightlightImages:(int)imageCount :(BOOL)value :(int)totalCount1
{
  
    
    if (value == YES)
    {
        
        label3.text = [NSString stringWithFormat:@"%d",totalCount1];
        switch (imageCount+1)
        {
            case 1:
                label1.text =@"1";
                [image1 setImage:[UIImage imageNamed:@"pageRed"]];
                
                break;
                
            case 2:
                label1.text =@"2";
                [image2 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
            case 3:
                label1.text =@"3";
                [image3 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
            case 4:
                label1.text =@"4";
                [image4 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
            case 5:
                label1.text =@"5";
                [image5 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
            case 6:
                label1.text =@"6";
                [image6 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
            case 7:
                label1.text =@"7";
                [image7 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
                
            case 8:
                label1.text =@"8";
                [image8 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
                
            case 9:
                label1.text =@"9";
                [image9 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
                
            case 10:
                label1.text =@"10";
                [image10 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
                
            case 11:
                label1.text =@"11";
                [image11 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
                
            case 12:
                label1.text =@"12";
                [image12 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
                
                
            case 13:
                label1.text =@"13";
                [image13 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 14:
                label1.text =@"14";
                [image14 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 15:
                label1.text =@"15";
                [image15 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 16:
                label1.text =@"16";
                [image16 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 17:
                label1.text =@"17";
                [image17 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 18:
                label1.text =@"18";
                [image18 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 19:
                label1.text =@"19";
                [image19 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
                
            case 20:
                label1.text =@"20";
                [image20 setImage:[UIImage imageNamed:@"pageRed"]];
                break;
            default:
                break;
        }

        
    }
    else
    {
            
       
        [image1 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image2 setImage:[UIImage imageNamed:@"pageGray"]];        
        
        [image3 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image4 setImage:[UIImage imageNamed:@"pageGray"]];
       
        [image5 setImage:[UIImage imageNamed:@"pageGray"]];
               
        [image6 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image7 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image8 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image9 setImage:[UIImage imageNamed:@"pageGray"]];
       
        [image10 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image11 setImage:[UIImage imageNamed:@"pageGray"]];
       
        [image12 setImage:[UIImage imageNamed:@"pageGray"]];
       
        [image13 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image14 setImage:[UIImage imageNamed:@"pageGray"]];
        
        
        [image15 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image16 setImage:[UIImage imageNamed:@"pageGray"]];
        
        
        [image17 setImage:[UIImage imageNamed:@"pageGray"]];
        
        
        [image18 setImage:[UIImage imageNamed:@"pageGray"]];
        
        [image19 setImage:[UIImage imageNamed:@"pageGray"]];
        
        
        [image20 setImage:[UIImage imageNamed:@"pageGray"]];
        
        

        label1.text =@"1";
        label2.text = @"/";
        label3.text = @"";
        
        
    }
  
}

#pragma mark - Convert m4a file into mp3 format

- (void)convertTapped:(NSDictionary *)info
{
    
    [NSThread detachNewThreadSelector:@selector(toMp3:) toTarget:self withObject:info];
}




- (void)toMp3:(NSDictionary *)info
{
    NSString *cafFilePath =[NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",[info valueForKey:@"FileName"]] ;
    NSString *mp3FileName = [info valueForKey:@"FileName"];
    mp3FileName = [mp3FileName stringByAppendingString:@".mp3"];
    NSString *mp3FilePath = [[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"] stringByAppendingPathComponent:mp3FileName];
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:cafFilePath];  
    if (exists)
    {
        @try {
            int read, write;
            
            FILE *pcm = fopen([cafFilePath cStringUsingEncoding:1], "rb");  //source
            fseek(pcm, 4*1024, SEEK_CUR);                                   //skip file header
            FILE *mp3 = fopen([mp3FilePath cStringUsingEncoding:1], "wb");  //output
            
            const int PCM_SIZE = 8192;
            const int MP3_SIZE = 8192;
            short int pcm_buffer[PCM_SIZE*2];
            unsigned char mp3_buffer[MP3_SIZE];
            
            lame_t lame = lame_init();
            lame_set_in_samplerate(lame, 44100.0);
            lame_set_VBR(lame, vbr_default);
            lame_init_params(lame);
            
            do {
                read = fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
                if (read == 0)
                    write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
                else
                    write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
                
                fwrite(mp3_buffer, write, 1, mp3);
                
            } while (read != 0);
            
            lame_close(lame);
            fclose(mp3);
            fclose(pcm);
        }
        @catch (NSException *exception) {
            NSLog(@"%@",[exception description]);
        }
        @finally {
            [self performSelectorOnMainThread:@selector(convertMp3Finish:)
                                   withObject:info
                                waitUntilDone:YES];
        }

        
    }
    
}


- (void) convertMp3Finish:(NSDictionary *)info
{
    NSLog(@"Finished Converting to Mp3");
    
    [self uploadFile:info];
   
}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag
{
   
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
 
}

#pragma mark - Upload mp3 file
-(void)uploadFile:(NSDictionary *)info
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *mp3FileName = nil;
    mp3FileName = [[info valueForKey:@"FileName"] stringByAppendingString:@".mp3"];
    NSString *mp3FilePath = [[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"] stringByAppendingPathComponent:mp3FileName];
    NSData *fileData = [NSData dataWithContentsOfFile:mp3FilePath];
    
    NSString *urlString1 = [baseURL stringByAppendingString:uploadRecordingURL];
        
    
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:urlString1]];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [defaults valueForKey:@"SessionId"],@"VSESSIONID",
                            [info valueForKey:@"FileName"],@"Filename",
                            nil];
    
    NSLog(@"%@",params);
    
       
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:urlString1 parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        
        
        [formData appendPartWithFileData:fileData name:@"Filedata" fileName:[info valueForKey:@"FileName"] mimeType:@"audio/mpeg"];
        
         
        
    }];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);

    }];
    
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        
         
         NSArray *stringPath;
         stringPath = [operation.responseString componentsSeparatedByString:@","];
         stringPath = [[stringPath objectAtIndex:1] componentsSeparatedByString:@":\""];
         stringPath = [[stringPath objectAtIndex:1] componentsSeparatedByString:@"\""];
         NSDictionary *tempDic = nil;
        
         
         
         
        [wordPathDictionary setValue:[info valueForKey:@"CurrentComponent"] forKey:@"gradedItem"];
         [wordPathDictionary setValue:durationValue forKey:@"duration"];
         [wordPathDictionary setValue:[stringPath objectAtIndex:0] forKey:@"fileName"];
        
         tempDic = [[NSDictionary alloc] initWithDictionary:wordPathDictionary];
         NSLog(@"%@",tempDic);
         [self addAllPaths:tempDic];
         tempDic = nil;
         
         if ([[wordDictionary valueForKey:@"lessonType"]intValue] == passage)
         {
           
             [self recordingCompleted];
         }
         
              
         
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
       
        NSLog(@"ERROR IN UPLOAD: %@, %@", operation, error);
        
        
        
    }];
    
    
    [operation start];
    
}




-(void)addAllPaths :(NSDictionary *)dic
{
   
    [pathArray addObject:dic];
    
    
}

-(void)uploadAllFiles
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];    
   // AppDelegate *appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   // endDate = [NSDate date];
   // NSTimeInterval diff = [endDate timeIntervalSinceDate:appdel.currentDate];
    NSString *url = [baseURL stringByAppendingString:uploadAllURL];
    
    totalRecordingTime = (curveInDuration + stopRecordingTime) * totalCount;
    NSLog(@"total time %f",totalRecordingTime);
    
    //{"recordingItems":[{"gradedItem":"stops","duration":2000,"fileName":"/services/resources/5e16676d-d120-4007-b3eb-f339609f6e18.mp3"}],"bundleUuid":"24c7175a-bdc3-21c2-f62e-f5090f14e7ef","timeOnTaskSeconds":128.652}
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            pathArray,@"recordingItems",
                            [defaults valueForKey:@"BundleUuid"],@"bundleUuid",
                             [NSString stringWithFormat:@"%f",totalRecordingTime],@"timeOnTaskSeconds",
                            nil];        
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"application/json"]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:url parameters:params];
    [request setTimeoutInterval:30.0];

        
   
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSLog(@"success words");
                                             [pathArray removeAllObjects];
                                             
                                             [defaults removeObjectForKey:@"RecordingInterrupted"];
                                             [defaults setBool:NO forKey:@"RecordingInterrupted"];
                                             [defaults synchronize];
                                             [self doneAllUploading];

                                                                                         
                                             
                                         } failure:^(NSURLRequest *request , NSURLResponse *response , NSError *error , id JSON)
                                         
                                         {
                                             NSLog(@"failure words %@ %@",error,[response description]);
                                             

                                                                                          
                                         }];
    [operation start];
    

    
}



#pragma mark - Authenticate User
-(void)cleanUp
{
    totalRecordingTime = 0;
    totalCount = 0;
    stopRecordingTime = 0;
    passageDuration = 0;
    fileName = nil;
    i = 0;
    micValue = 0;
    [self hightlightImages:0 :NO :0];
    [pathArray removeAllObjects];
    [wordDictionary removeAllObjects];
    wordCount = 0;
    NSString *filePath =[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:nil];

    
}
-(void)authenticateUser
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *url = [baseURL stringByAppendingString:authenticationURL];    
    if ([defaults valueForKey:@"SessionId"] != nil)
    {
       url = [url stringByAppendingFormat:@"VSESSIONID=%@",[defaults valueForKey:@"SessionId"]];
    }
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:url parameters:nil];
    [request setTimeoutInterval:30.0];
    
      AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                            
                                             
                                            
                                             if ([JSON isKindOfClass:[NSDictionary class]])
                                             {
                                                 if ([[JSON valueForKey:@"success"] intValue] == 1)
                                                 {
                                                     
                                                     NSDictionary *dic = [[JSON valueForKey:@"messages"] objectAtIndex:0];
                                                     if ([[dic valueForKey:@"description"] isEqualToString:@"OK"])
                                                     {
                                                         [self fetchWords];
                                                     }

                                                 }
                                                 else
                                                 {
                                                     if ([[JSON valueForKey:@"success"] intValue] == 0)
                                                     {
                                                         NSDictionary *dic = [[JSON valueForKey:@"messages"] objectAtIndex:0];
                                                         
                                                         UIAlertView *authenticationAlert = [[UIAlertView alloc]
                                                                                             initWithTitle: @"Authentication Fail"
                                                                                             message:[dic valueForKey:@"description"]                                                                                      delegate: self
                                                                                             cancelButtonTitle:@"OK"
                                                                                             otherButtonTitles:nil];
                                                         [authenticationAlert show];
                                                         
                                                     }
 
                                                 }
                                                 
                                              
                                             }
                                             
                                             
                                                                                      
                                             
                                         } failure:^(NSURLRequest *request , NSURLResponse *response , NSError *error , id JSON)
                                         
                                         {
                                             
                                                     UIAlertView *authenticationAlert = [[UIAlertView alloc]
                                                                                         initWithTitle: @"Authentication Fail"
                                                                                         message:@""                                                                                  delegate: self
                                                                                         cancelButtonTitle:@"OK"
                                                                                         otherButtonTitles:nil];
                                                     [authenticationAlert show];
                                                     
                                                                                            
                                         }];
    [operation start];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 0)
    {
        NSString *url1 = redirectURL;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url1]];
    }
}


#pragma mark - Parse words phoneme senrences passage
-(void)fetchWords
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    wordDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];     
    NSString *url =  [baseURL stringByAppendingString:recordingPractiseURL];
    url = [url stringByAppendingFormat:@"bundleUUID=%@",[defaults valueForKey:@"BundleUuid"]];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:url parameters:nil];
    [request setTimeoutInterval:30.0];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    //application/json
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
   
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSLog(@"success words");
                                             
                                             NSLog(@"Words %@",JSON);
                                             
                                             if ([[JSON objectForKey:@"success"] intValue] == 1)
                                             {
                                                 if ([JSON isKindOfClass:[NSDictionary class]])
                                                 {
                                                     [wordDictionary setDictionary:JSON];
                                                     
                                                    [self downloadingComplete];
                                                 }
                                             }
                                            
                                            
                                             
                                         } failure:^(NSURLRequest *request , NSURLResponse *response , NSError *error , id JSON)
                                         
                                         {
                                             NSString *url1 = redirectURL;
                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url1]];
                                             
                                         }];
    [operation start];
    

}

-(void)my_viewWillUnload
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    passageDuration = 0;
    totalRecordingTime = 0;
    stopRecordingTime = 0;
    passageValue = 0;
    fileName = nil;
    endDate = nil;
    totalCount = 0;
    if (recorder) {
        [recorder stop];
        recorder.delegate = nil;
        recorder = nil;
    }    i = 0;
    micValue = 0;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    [self hightlightImages:0 :NO :0];
    [pathArray removeAllObjects];
    [wordDictionary removeAllObjects];
    wordCount = 0;
    NSString *filePath =[NSHomeDirectory() stringByAppendingFormat:@"/Documents/"];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:nil];
    [[NSThread currentThread] cancel];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight ||
            interfaceOrientation == UIInterfaceOrientationLandscapeLeft );
}


@end
