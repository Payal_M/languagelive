//
//  AppDelegate.h
//  Cambium
//
//  Created by ScriptLanes on 15/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultScreenViewController.h"


@class ViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSDate *currentDate;

@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) DefaultScreenViewController *defaultviewController;

@end
