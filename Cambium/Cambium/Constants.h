//
//  Constants.h
//  Cambium
//
//  Created by ScriptLanes on 19/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//


#pragma mark - Notifications

extern NSString * authenticationURL ;

extern NSString * productionURL;

extern NSString * baseURL ;



extern int const  words ;
extern int const phonemes ;
extern int const sentences;
extern int const passage;

extern float const curveInDuration;
extern float const curveOutDuration;


extern NSString * recordingPractiseURL ;
extern NSString * uploadRecordingURL ;
extern NSString * uploadAllURL ;
extern NSString * reviewURL;
extern NSString * redirectURL ;
