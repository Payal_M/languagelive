//
//  Constants.h
//  Cambium
//
//  Created by ScriptLanes on 19/07/13.
//  Copyright (c) 2013 ScriptLanes. All rights reserved.
//


#import "Constants.h"


// Created Dynamically baseURL + VSessionID
 NSString * authenticationURL = @"";

//Launch Safari IF App is Launched VIA Tapping Language Learning App ICON over iPAD
 NSString * productionURL = @"https://languagelive.voyagerlearning.com";

// Stored Dynamically when invoked from SAFARI from iPAD
 NSString * baseURL = @"";


// Lesson Type For launching corresponding Recording Activities 
int const words = 3;
int const phonemes = 2;
int const sentences = 8;
int const passage = 9;
float const curveInDuration = 0.2;
float const curveOutDuration = 0.2;


// Service URL's to be appended using BaseURL 
 NSString * recordingPractiseURL =@"/services/partner/recordingPractice?";
 NSString * uploadRecordingURL = @"/services/partner/uploadRecording";
 NSString * uploadAllURL = @"/services/partner/saveUngradedRecordings";
 NSString * reviewURL = @"/assignment/?view=dashboard";

// Created Dynamically when invoked from SAFARI from iPAD
 NSString * redirectURL = @"";

